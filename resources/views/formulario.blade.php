@extends('layouts.base',['title'=> 'Formulario'])

@section('body')
    <p style="color: #3d6983"><strong>Log in</strong></p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <u1>
                @foreach($errors->all() as $error)
                    <p>{{$errors}}</p>
                @endforeach
            </u1>
        </div>
    @endif
    <form action="{{url('/')}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <label for="user">Username</label>
        <input type="text" name="user" id="user" placeholder="Inser Username" value="{{old('user')}}"><br><br>
        <label for="pass">Password</label>
        <input type="text" name="pass" id="pass" placeholder="Insert Password" value="{{old('pass')}}"><br><br>
        <label for="poster">Foto del usuario</label>
        <input type="file" name="poster" id="poster"><br><br>
        <input type="submit" value="Send">
    </form>
    @stop
