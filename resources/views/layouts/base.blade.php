<!doctype html>
<html lang="es">
<head>
 <meta charset="UTF-8">
 <title>{{$title}}</title>
 @section ('estilos')
 <link rel="stylesheet" href="estilos.css" />
 @show
</head>
<body>
 @include('cabecera')
 @yield('body')
@include('pie')
</body>
</html>