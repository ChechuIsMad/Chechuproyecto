@extends('layouts.base', ['title'=> 'Peliculas'])
@section('estilos')
    @parent
    <link rel="stylesheet" href="peliculas.css">
@stop

@section('body')
 <p>Películas de <strong>{{$genero}}</strong></p>
@stop
