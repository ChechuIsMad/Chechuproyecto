<?php
Route::get('pagina/prueba', function(){
	return "Hola mundo!";
});
Route::get('pagina/primera', function(){
	return "Primera pagina";
});
Route::get('pagina/segunda', function(){
	return "Segunda pagina";
});
Route::get('pagina/tercera', function(){
	return "Tercera pagina";
});
Route::get('/', function(){
    $datos = Request::only('var1');
    return $datos;
	//return View::make('basico');
});

Route::get('indice/{genero}', function($genero){
	return View::make('peliculas', compact('genero'));
});
Route::get('primera', function () {
 return Redirect::to('segunda');
});
Route::get('segunda', function () {
 return "Segunda entrada";
});
Route::get('prueba/json', function(){
	$datos = [
		'cursos' => [
			'laravel',
			'angular',
		] ,
		'alumnos' => [
			'pepe',
			'manolo',
			'andres',
			'bertin',
		]
	];
	return Response::json($datos);
});
Route::get('prueba/descarga',function(){
	$file = storage_path().'\app\public\google2.0.0.jpg';
	return Response::download($file, 'google2.0.0.jpg');
});
Route::get('prueba/formulario',function () {
    return View::make('formulario');
});
Route::post('/',function(){
    Request::validate(['user' => 'required|string|max:10',
        'pass' => 'required|string|min:4|max:8',
        'poster' => 'image'
    ]);

    $archivo=Request::file('poster');
    $nombreArchivo=$archivo->getCLientOriginalName();
    $archivo->move(public_path()."/imagenes",$nombreArchivo);

    return Request::all();

});